var productosObtenidos;
var clientesObtenidos;

function getProductos() {
  var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
        console.log(request.responseText);
        productosObtenidos = request.responseText;
        procesarProductos();
     }
    }
    request.open("GET",url,true);
    request.send();
}

function procesarProductos(){
   var JSONProductos = JSON.parse(productosObtenidos);
   //alert("Nombre: " + JSONProductos.value[0].ProductName);

   var divTabla = document.getElementById("divTabla");
   var tabla = document.createElement("table");
   var tbody = document.createElement("tbody");
   tabla.classList.add("table");
   tabla.classList.add("table-striped");

   for (var i = 0; i < JSONProductos.value.length; i++) {
     var nuevaFila = document.createElement("tr");

     var columnaNombre = document.createElement("td");
     columnaNombre.innerText = JSONProductos.value[i].ProductName;
     var columnaPrecio = document.createElement("td");
     columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
     var columnaStock = document.createElement("td");
     columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

     nuevaFila.append(columnaNombre);
     nuevaFila.append(columnaPrecio);
      nuevaFila.append(columnaStock);

      tbody.appendChild(nuevaFila);
     //tabla.appendChild(nuevaFila);
     //console.log( i + ": " + JSONProductos.value[i].ProductName );
   }
   tabla.appendChild(tbody);
   divTabla.append(tabla);
}


function getCustomers() {
  //var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
        console.log(request.responseText);
        clientesObtenidos = request.responseText;
        procesarClientes();
     }
    }
    request.open("GET",url,true);
    request.send();
}


//nombre ,ciudad, telefono y pais

function procesarClientes(){
   var JSONClientes = JSON.parse(clientesObtenidos);
   //alert("Nombre: " + JSONProductos.value[0].ProductName);
   var rutaBandera="https://www.countries-ofthe-world.com/flags-normal/flag-of-";

//https://www.countries-ofthe-world.com/flags-normal/flag-of-Afghanistan.png

   var divTabla = document.getElementById("divTablaCliente");
   var tabla = document.createElement("table");
   var tbody = document.createElement("tbody");
   tabla.classList.add("table");
   tabla.classList.add("table-striped");

   for (var i = 0; i < JSONClientes.value.length; i++) {
     var nuevaFila = document.createElement("tr");

     var columnaNombre = document.createElement("td");
     columnaNombre.innerText = JSONClientes.value[i].ContactName;

     var columnaCountry = document.createElement("td");
     //columnaCountry.innerText = JSONClientes.value[i].Country;
     var imgBandera= document.createElement("img");
     imgBandera.classList.add("flag");
     if(JSONClientes.value[i].Country=="UK"){
       imgBandera.src=rutaBandera + "United-Kingdom.png";
     }else{
        imgBandera.src=rutaBandera + JSONClientes.value[i].Country +".png";
     }
     columnaCountry.appendChild(imgBandera);

     var columnaPhone = document.createElement("td");
     columnaPhone.innerText = JSONClientes.value[i].Phone;
     var columnaCity = document.createElement("td");
     columnaCity.innerText = JSONClientes.value[i].City;

     nuevaFila.append(columnaNombre);
     nuevaFila.append(columnaCity);
     nuevaFila.append(columnaPhone);
     nuevaFila.append(columnaCountry);

      tbody.appendChild(nuevaFila);
     //tabla.appendChild(nuevaFila);
     //console.log( i + ": " + JSONProductos.value[i].ProductName );
   }
   tabla.appendChild(tbody);
   divTabla.append(tabla);
}
